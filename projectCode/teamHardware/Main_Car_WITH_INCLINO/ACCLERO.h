#ifndef Acclero
#define Acclero

#include "Arduino.h"

#include "I2Cdev.h"



#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif

#endif

class ACCLERO
{ 

  public:

    ACCLERO();

    void SETUP();
    void Wire_ACCLERO();
    int16_t Read_ACCLERO();
    double INCLINOMETER();

    double value;

    

   private:

    void SETGYRO();
    void devStatus_True();
    void dmpDataReady();
    void devStatus_False();

    const int MPU_addr=0x68;
    bool dmpReady = false; 
    bool mpuInterrupt = false; 
    float euler[3],ypr[3];

    int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;
    uint8_t mpuIntStatus,devStatus,packetSize,fifoCount,fifoBuffer[64];
    uint8_t teapotPacket[14];
};

