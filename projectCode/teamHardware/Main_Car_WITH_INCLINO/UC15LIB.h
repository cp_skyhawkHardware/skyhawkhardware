#ifndef UC15
#define UC15

#include "Arduino.h"

#include "TEE_UC20.h"
#include "SoftwareSerial.h"
#include <AltSoftSerial.h>
#include "internet.h"
#include "uc_mqtt.h"
#include "stdio.h"

#endif

class COMMUNICATION
{

  public:

    //constructor function

    COMMUNICATION();
    

    //Setup for connection

    void SETUP(void (*callback)(String ,char*,unsigned char));
    void NET(String apn,String user,String pass,int pwrkey);
    void MQTT(String mqtt_server,String mqtt_port,String mqtt_id,String mqtt_user,String mqtt_password,String mqtt_subscriber);
    void UPLOAD(String Topic,String Info,boolean Show);
    void MqttLoop();
    bool Connectstate();
    void ConnectServer (void (*callback)(String ,char*,unsigned char));


    //setting up Phase
    
  private:
  
    void TriggerUC15();
    void NETSETUP();
    void SERVERSETUP();
    void CONNECTSERVER(void (*callback)(String ,char*,unsigned char));
    //void Debug(String Data);

    String MQTT_PASSWORD;
    String MQTT_SERVER;
    String MQTT_PORT;
    String MQTT_ID;
    String MQTT_USER;
    String MQTT_Subscriber;
    unsigned char ret;
    const char *id,*user,*pass,*sub;

    

    String APN,USER,PASS;
    int PWRKEY;
    bool _msg;
  
};


