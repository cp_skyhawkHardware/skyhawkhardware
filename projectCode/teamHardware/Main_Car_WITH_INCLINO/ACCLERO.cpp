#include "ACCLERO.h"
#include "MPU6050_6Axis_MotionApps20.h"
#define OUTPUT_READABLE_YAWPITCHROLL

MPU6050 mp;

const int MPU_addr=0x68;
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;
bool dmpReady = false; 
uint8_t mpuIntStatus;
uint8_t devStatus; 
uint16_t packetSize;
uint16_t fifoCount; 
uint8_t fifoBuffer[64];

Quaternion q; // [w, x, y, z] quaternion container
VectorInt16 aa; // [x, y, z] accel sensor measurements
VectorInt16 aaReal; // [x, y, z] gravity-free accel sensor measurements
VectorInt16 aaWorld; // [x, y, z] world-frame accel sensor measurements
VectorFloat gravity; // [x, y, z] gravity vector
float euler[3]; // [psi, theta, phi] Euler angle container
float ypr[3]; // [yaw, pitch, roll] yaw/pitch/roll container and gravity vector

uint8_t teapotPacket[14] = { '$', 0x02, 0,0, 0,0, 0,0, 0,0, 0x00, 0x00, '\r', '\n' };
volatile bool mpuInterrupt = false; 
void dmpDataReady() {
  mpuInterrupt = true;
}

ACCLERO::ACCLERO()
{

  
}

void ACCLERO::SETUP()
{
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
  Wire.begin();
  TWBR = 24; // 400kHz I2C clock (200kHz if CPU is 8MHz)
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
  Fastwire::setup(400, true);
  #endif
  
  Serial.begin(9600);
  while (!Serial); 
  Serial.println(F("Initializing I2C devices..."));
  mp.initialize();
  Serial.println(F("Testing device connections..."));
  Serial.println(mp.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

  Serial.println(F("Initializing DMP..."));
  devStatus = mp.dmpInitialize();

  SETGYRO();
  
  if (devStatus == 0) { 
    
    devStatus_False();
  } else { 
    
    devStatus_True();
  }
}

void ACCLERO::SETGYRO()
{
   // supply your own gyro offsets here, scaled for min sensitivity
  
   mp.setXGyroOffset(220);
   mp.setYGyroOffset(76);
   mp.setZGyroOffset(-85);
   mp.setZAccelOffset(1788); // 1688 factory default for my test chip

    
}

void ACCLERO::devStatus_True()
{
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));
}

void ACCLERO::dmpDataReady() {
  mpuInterrupt = true;
}

void ACCLERO::devStatus_False()
{
  Serial.println(F("Enabling DMP..."));
    mp.setDMPEnabled(true);

    Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));


    attachInterrupt(0,reinterpret_cast<void (*)()>(&dmpDataReady) , RISING);
    mpuIntStatus = mp.getIntStatus();
    
    Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;
    
    packetSize = mp.dmpGetFIFOPacketSize();
}

void ACCLERO::Wire_ACCLERO()
{
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
}

double ACCLERO::INCLINOMETER()
{
    mp.dmpGetQuaternion(&q, fifoBuffer);
    mp.dmpGetGravity(&gravity, &q);
    mp.dmpGetYawPitchRoll(ypr, &q, &gravity);

    value = 1545 + abs(ypr[1]/M_PI)*1710;

    return(value);
}

int16_t ACCLERO::Read_ACCLERO(){

  return(Wire.read()<<8|Wire.read());
}

