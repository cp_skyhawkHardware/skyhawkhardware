//warning
//should disconnect net and mcqtt first then set thing up
#include "UC15LIB.h"
#include "ACCLERO.h"
int16_t acX,acY,acZ,tmp,gyX,gyY,gyZ;

UCxMQTT mqtt;
unsigned long previousmqtt = 0;
const long intervalmqtt = 5000;
int a = 1;
double Inclino;

ACCLERO AC;
COMMUNICATION uc;

void setup() {
  // put your setup code here, to run once:
   Serial.begin(9600);
   
   uc.NET("internet","","",42);
   uc.MQTT("35.187.242.237","1883","device","staff","51<yk@3k2o18","skyhawkPhase1");

   uc.SETUP(callback);
   uc.UPLOAD("skyhawkPhase1","Hello from device",0);
  
   AC.SETUP();

   RTC.begin();
   if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    RTC.adjust(DateTime(__DATE__, __TIME__));
   }
   
}

void callback(String topic ,char *payload,unsigned char length)
{
  Serial.println();
  Serial.println(F("******************"));
  Serial.print(F("Topic = "));
  Serial.println(topic);
  payload[length]=0;
  String str_data(payload);
  Serial.print(F("Payload = "));
  Serial.println(str_data);
}

void loop(){

  unsigned long currentMillis = millis();
  DateTime now = RTC.now(); 
  AC.Wire_ACCLERO();
  if(currentMillis - previousmqtt >= intervalmqtt)  //every 5 sec publish
  {
  
      acX=AC.Read_ACCLERO();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
      acY=AC.Read_ACCLERO();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
      acZ=AC.Read_ACCLERO();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
      tmp=AC.Read_ACCLERO();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
      gyX=AC.Read_ACCLERO();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
      gyY=AC.Read_ACCLERO();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
      gyZ=AC.Read_ACCLERO();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
      Inclino = AC.INCLINOMETER();

      String t = "AcX = " + String(acX) + " | AcY = " + String(acY) + " | AcZ = " + String(acZ) + " | Tmp = " + String(tmp/340.00+36.53) + " | GyX = " + String(gyX) + " | GyY = " + String(gyY) + " | GyZ = " + String(gyZ) + " | Inclino = " + String(Inclino);
      Serial.println(t);
  
      uc.UPLOAD("skyhawkPhase1",t,0);
      Serial.println("Sending device status to server") ;
      previousmqtt = currentMillis; 
  }
  uc.MqttLoop();
  if(!uc.Connectstate())      // check server
     uc.ConnectServer(callback);  
}
