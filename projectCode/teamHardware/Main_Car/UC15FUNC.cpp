#include "UC15LIB.h"
#include "TEE_UC20.h"
#include "SoftwareSerial.h"
#include <AltSoftSerial.h>
#include "internet.h"
#include "uc_mqtt.h"
#include "stdio.h"

//create Object For internet
AltSoftSerial mySerial;
INTERNET net;
UCxMQTT mcqtt;
int PWRKEY;
bool _msg;

//net
String APN,USER,PASS;

//mqtt
String MQTT_PASSWORD;
String MQTT_SERVER;
String MQTT_PORT;
String MQTT_ID;
String MQTT_USER;
String MQTT_Subscriber;

COMMUNICATION::COMMUNICATION()
{
  
}

void COMMUNICATION::NET(String apn,String user,String pass,int pwrkey){

  APN = apn;
  USER = user;
  PASS = pass;
  PWRKEY = pwrkey;
}
void COMMUNICATION::MQTT(String mqtt_server,String mqtt_port,String mqtt_id,String mqtt_user,String mqtt_password,String Subscriber){

  MQTT_PASSWORD = mqtt_password;
  MQTT_SERVER = mqtt_server;
  MQTT_PORT = mqtt_port;
  MQTT_ID = mqtt_id;
  MQTT_USER = mqtt_user;
  MQTT_PASSWORD = mqtt_password;
  MQTT_Subscriber = Subscriber;
}
void COMMUNICATION::SETUP(void (*callback)(String ,char*,unsigned char)){


  Serial.print("Trigger UC15...");
  TriggerUC15();

  Serial.begin(9600);
  gsm.begin(&mySerial,9600); //open communicate
  Serial.print("Connecting...");

  while(gsm.WaitReady()){Serial.println("Wait");} //wait until gsm module set

  NETSETUP();
  SERVERSETUP();
  CONNECTSERVER(callback);
} 

void COMMUNICATION::SERVERSETUP(){
  
  net.DisConnect();
  net.Configure(APN,USER,PASS); //from Pg34
  net.Connect();  //connect net
  Serial.print("IP:");
  Serial.println(net.GetIP());

 // mcqtt.callback = callback; //?? Pg50 -> info that sent from server will be add in this function as -> topic,info,size -> everything come as char 

  //Connect_Server(); //start connect server
}

void COMMUNICATION::NETSETUP(){

  Serial.print("OPERATOR : ");
  Serial.println(gsm.GetOperator()); //Operator -> AIS TRUE DTAC
  Serial.print("Signal Strength");
  Serial.println(gsm.SignalQuality()); //STRENGTH -> Pg22,0 = good,99 = bad
}
void COMMUNICATION::CONNECTSERVER(void (*callback)(String ,char *,unsigned char)){

  //Connect server

  const char *id = &MQTT_ID[0];
  const char *user = &MQTT_USER[0];
  const char *pass = &MQTT_PASSWORD[0];
  const char *sub = &MQTT_Subscriber[0];
  
  do{
    Serial.println("Start Connect Server");

    if(mcqtt.DisconnectMQTTServer()){ //Disconnect mqtt server first

      mcqtt.ConnectMQTTServer(MQTT_SERVER,MQTT_PORT); //connect via destinated server and 
    }
    delay(500); //wait for proceed
    Serial.println(mcqtt.ConnectState()); //Show Connection (boolean)
    
  }while(!mcqtt.ConnectState());//until connect succed,keep trying
  
  Serial.println("SERVER CONNECT");

  unsigned char ret = mcqtt.Connect(id,user,pass); //change username to boolean

  //login username,password

  Serial.println(mcqtt.ConnectReturnCode(ret));

  if(ret == 0){ //if connect

    mcqtt.Subscribe(sub); 
    mcqtt.callback = callback;
  }
}
void COMMUNICATION::MqttLoop(){
  mcqtt.MqttLoop();
}
//

//
//  Serial.println(mcqtt.ConnectReturnCode(ret));  //return boolean if it connect or not
//
//  if(ret == 0){ //if connect
//
//    mcqtt.Subscribe(MQTT_Subscriber);
//  }
//}

bool COMMUNICATION::UPLOAD(String Topic,String Info,boolean Show){

  return mcqtt.Publish(Topic,Info,Show); 
  
}

bool COMMUNICATION::Connectstate(){

  return(mcqtt.ConnectState());
  
}

void COMMUNICATION::ConnectServer(void (*callback)(String ,char *,unsigned char)){

  CONNECTSERVER(callback);
  
}

void COMMUNICATION::TriggerUC15(){

  pinMode(PWRKEY,OUTPUT);
  digitalWrite(PWRKEY,HIGH);
  delay(3000);
  digitalWrite(PWRKEY,LOW);
}


