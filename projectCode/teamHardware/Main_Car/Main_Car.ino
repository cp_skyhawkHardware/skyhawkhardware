#include <ServoTimer2.h>

//warning
//should disconnect net and mcqtt first then set thing up
#include "UC15LIB.h"
#include "ACCLERO.h"
#include "RTClib.h"
#include <ArduinoJson.h>
#include <TaskScheduler.h>
#include <Wire.h>

//int16_t aX, aY, aZ, gyX, gyY, gyZ, angleX, angleY, angleZ;

UCxMQTT mqtt;
const long reportInterval = 1000;
bool isSystemWasInitiated = false;
String tripID = "";

ACCLERO AC;
COMMUNICATION uc;
ServoTimer2 BR1306;
RTC_DS1307 RTC;

//Function prototype
void reportDeviceStatus();

//Task list
Task reportStatus(reportInterval, TASK_FOREVER, &reportDeviceStatus);

//Scheduler
Scheduler runner;

//MQTT Receive and Handle Example Code : Set throttle for brushless motor
void MotorSetup() {
  Serial.println("Setting up motor...");
  BR1306.attach(9);
  BR1306.write(700);
  delay(2000);
}
void MotorThrottle(int Throttle) {
  Serial.println(Throttle);
  BR1306.write(Throttle);
}

void InitiateSystem(String newTripID){
  tripID = newTripID;
  isSystemWasInitiated = true;
}
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Wire.begin();
  RTC.begin();
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  uc.NET("internet", "", "", 42);
  uc.MQTT("35.187.242.237", "1883", "device", "staff", "51<yk@3k2o18", "skyhawkPhase1");

  uc.SETUP(receiveCommand);

  AC.SETUP();
  runner.init();
  runner.addTask(reportStatus);
  uc.UPLOAD("skyhawkPhase1", "{\"dataType\":\"Greeting\"}", true);
}

void receiveCommand(String topic , char *payload, unsigned char length)
{
  const char* serverCommand;
  payload[length] = 0;
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(payload);
  if (root.success()) {
    serverCommand = root["Command"];
    Serial.print("Server Command: ");
    Serial.println(serverCommand);
    if (strcmp(serverCommand, "newTripID") == 0) {
      InitiateSystem(root["Payload"]);
    } 
    if(isSystemWasInitiated){
       if (strcmp(serverCommand, "throttle") == 0) {
        MotorThrottle(root["Payload"]);
       } 
    }
  }
}

void reportDeviceStatus() {
  String jsonData;
  StaticJsonBuffer <200> jsonBuffer;

  JsonObject& root = jsonBuffer.createObject();
  root["dataType"] = "DeviceStatus";
  JsonObject& payload = root.createNestedObject("payload");
  payload["flightID"] = tripID;
  payload["aX"] = AC.aX; payload["aY"] = AC.aY; payload["aZ"] = AC.aZ;
  payload["angleX"] = AC.angleX; payload["angleY"] = AC.angleY; payload["angleZ"] = AC.angleZ;
  payload["gyroX"] = AC.gyX; payload["gyroY"] = AC.gyY; payload["gyroZ"] = AC.gyZ;
  DateTime now = RTC.now();
  payload["timestamp"] = now.unixtime();
  root.printTo(jsonData);
  //  Serial.print("Sending device status to server: ");
  Serial.println(uc.UPLOAD("skyhawkPhase1",jsonData, false));
}

void loop() {
  if(isSystemWasInitiated){
    reportStatus.enable();
  }
  AC.UPDATE();
  runner.execute();
  uc.MqttLoop();
  if (!uc.Connectstate())     // check server
    uc.ConnectServer(receiveCommand);
}
