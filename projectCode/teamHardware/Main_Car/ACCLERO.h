#ifndef Acclero
#define Acclero

#include "Arduino.h"

#include "I2Cdev.h"



#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif

#endif

class ACCLERO
{ 

  public:

    ACCLERO();

    void SETUP();
    void UPDATE();
    int16_t aX,aY,aZ,gyX,gyY,gyZ, angleX, angleY, angleZ;
    
   private:

    void SETGYRO();
    void incomingData();
    void devStatus_True();
    void dmpDataReady();
    void devStatus_False();

    const int MPU_addr=0x68;
    bool dmpReady = false; 
    bool mpuInterrupt = false; 
    float euler[3],ypr[3];

    uint8_t mpuIntStatus,devStatus,packetSize,fifoCount,fifoBuffer[64];
    uint8_t teapotPacket[14];
    
    void READ_LINEAR_ACCELERATION();
    void READ_ANGLE_ACCELERATION();
    void READ_ANGLE_TILT();
};

