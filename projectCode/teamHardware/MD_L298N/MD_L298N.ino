#include "MD_L298N.h"

MotorDriver mDriver;

float prevSpeed,Speed;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  mDriver.Attach(10, 30, 31, 11, 32, 33);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (prevSpeed != Speed){

    mDriver.Drive(Speed);
    prevSpeed = Speed;

    Serial.println(Speed);
  }
}
