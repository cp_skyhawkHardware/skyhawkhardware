#ifndef MD_L298N
#define MD_L298N

#include "Arduino.h"

class MotorDriver
{

	private:

		int analogSpeed; // Converted Speed ranged 0-255
		int ENA, IN1, IN2; // WheelA
		int ENB, IN3, IN4; // WheelB
		

	public:

		float CurrentSpeed;
		float MaxSpeed;

		void Attach(int enA, int in1, int in2,
					int enB, int in3, int in4);

		void Drive(float Speed);
		
};
#endif // MD_L298N
