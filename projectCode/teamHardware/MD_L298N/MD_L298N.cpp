#include "Arduino.h"
#include "MD_L298N.h"

int analogSpeed; // Converted Speed ranged 0-255
int ENA, IN1, IN2; // WheelA
int ENB, IN3, IN4; // WheelB

float CurrentSpeed;
float MaxSpeed;

void MotorDriver::Attach(int enA, int in1, int in2,
			int enB, int in3, int in4)
{

	ENA = enA;
	IN1 = in1;
	IN2 = in2;
	ENB = enB;
	IN3 = in3;
	IN4 = in4;

	// Set pinMode
	pinMode(ENA, OUTPUT);
	pinMode(IN1, OUTPUT);
	pinMode(IN2, OUTPUT);
	pinMode(ENB, OUTPUT);
	pinMode(IN3, OUTPUT);
	pinMode(IN4, OUTPUT);

};


void MotorDriver::Drive(float Speed)
{

	if (Speed > 0) {

		// Forward Speed
		if (Speed > MaxSpeed) {

			Speed = MaxSpeed;

		}

		CurrentSpeed = Speed;
		analogSpeed = map(CurrentSpeed, 0, MaxSpeed, 0, 255);

		// set WheelA to ForwardRun
		digitalWrite(IN1, HIGH);
		digitalWrite(IN2, LOW);

		// set WheelB to ForwardRun
		digitalWrite(IN3, HIGH);
		digitalWrite(IN4, LOW);

		// write analogSpeed
		analogWrite(ENA, analogSpeed);
		analogWrite(ENB, analogSpeed);

	}

	else if (Speed < 0) {

		if (Speed < -MaxSpeed) {

			Speed = -MaxSpeed;

		}

		CurrentSpeed = Speed;
		analogSpeed = map(CurrentSpeed, -MaxSpeed, 0, 255, 0);

		// set WheelA to BackwardRun
		digitalWrite(IN1, LOW);
		digitalWrite(IN2, HIGH);

		// set WheelB to BackwardRun
		digitalWrite(IN3, LOW);
		digitalWrite(IN4, HIGH);

		// write analogSpeed
		analogWrite(ENA, analogSpeed);
		analogWrite(ENB, analogSpeed);

	}
};
