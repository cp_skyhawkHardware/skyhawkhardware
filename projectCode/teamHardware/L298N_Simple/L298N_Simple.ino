#include <L298N.h>

//pin definition
#define ENA 3
#define IN1 4
#define IN2 5

#define ENB 8
#define IN3 6
#define IN4 7

//create a motor instance
L298N motorA(ENA, IN1, IN2);
L298N motorB(ENB, IN3, IN4);

void setup() {

  //used for display information
  Serial.begin(9600);

  motorA.setSpeed(80); // an integer between 0 and 255
  motorB.setSpeed(80); // an integer between 0 and 255

}

void loop() {

  //tell the motor to go forward (may depend by your wiring)
  motorA.forward();
  motorB.forward();

//  //print the motor satus in the serial monitor
//  Serial.print("Is moving = ");
//  Serial.println(motorA.isMoving());
//  Serial.println(motorB.isMoving());
//
//  delay(3000);
//
//  //stop running
//  motorA.stop();
//  motorB.stop();
//
//  Serial.print("Is moving = ");
//  Serial.println(motor.isMoving());
//
//  delay(3000);
//
//  //change the initial speed
//  motor.setSpeed(100);
//
//  //tell the motor to go back (may depend by your wiring)
//  motor.backward();
//
//  Serial.print("Is moving = ");
//  Serial.println(motor.isMoving());
//
//  delay(3000);
//
//  //stop running
//  motor.stop();
//
//  Serial.print("Is moving = ");
//  Serial.println(motor.isMoving());
//
//  //change the initial speed
  motorA.setSpeed(255);
  motorB.setSpeed(255);

  Serial.print("Get new speed = ");
  Serial.println(motorA.getSpeed());
  Serial.println(motorB.getSpeed());


  delay(3000);
}
