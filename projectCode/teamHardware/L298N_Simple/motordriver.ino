class motor
{

    private:
    
        // ports
        int ENA, IN1, IN2;
      
    public:
    
        float Speed, maxSpeed, dir;
        int analogSpeed;
      
        void Attach(int en, int in1, int in2)
        {
        
            // set ports
            ENA = en;
            IN1 = in1;
            IN2 = in2;
            
            pinMode(ENA, OUTPUT);
            pinMode(IN1, OUTPUT);
            pinMode(IN2, OUTPUT);
            
            Speed = 0;
            dir = 1;
            analogSpeed = 0;
            
        };
        
        int setSpeed(float newSpeed)
        {
        
            // update Speed
            if(newSpeed < 0)
            {
                Speed = -map(newSpeed, 0, maxSpeed, 0, maxSpeed);
                dir = -1;
            }
            else
            {
                Speed = map(newSpeed, 0, maxSpeed, 0, maxSpeed);
                dir = 1;
            }
            analogSpeed = int(map(Speed, 0, maxSpeed, 0, 255));
            
            // get direction
            if(dir == 1)
            {
                digitalWrite(IN1, HIGH);
                digitalWrite(IN2, LOW);
            }
            else if(dir == -1)
            {
                digitalWrite(IN1, LOW);
                digitalWrite(IN2, HIGH);
            }
            else
            {
                return 0;
            }
            
            analogWrite(analogSpeed);
            
        };
        
};

motor LeftMotor;
motor RightMotor;

void setup()
{
    
    LeftMotor.Attach(,,);
    RightMotor.Attach(,,);
    LeftMotor.maxSpeed = ;
    RightMotor.maxSpeed = ;
    
}

void loop()
{

    LeftMotor.setSpeed();
    RightMotor.setSpeed();
    
}