//warning
//should disconnect net and mcqtt first then set thing up

#define OUTPUT_READABLE_YAWPITCHROLL

#include "TEE_UC20.h"
#include "SoftwareSerial.h"
#include <AltSoftSerial.h>
#include "internet.h"
#include "uc_mqtt.h"
#include "stdio.h"
INTERNET net; //create object net for connect internet
UCxMQTT mcqtt; //create mqtt to recieve + transfer

//INTERNET -> AIS 3G

#define APN "internet"
#define USER ""
#define PASS ""
#define PWRKEY 42

//SERVER TIGER

#define MQTT_SERVER      "35.188.255.63"
#define MQTT_PORT        "1883"
#define MQTT_ID          "device"
#define MQTT_USER        ""
#define MQTT_PASSWORD    ""

unsigned long previousmqtt = 0;
const long intervalmqtt = 5000;
int a = 1;
//int LEDPIN = 48;

AltSoftSerial mySerial; //set port tranfer 46 recieve 48

void dmpDataReady() {
}

void setup() {
  // put your setup code here, to run once:


  Serial.print("OPERATOR : ");
  triggerUC15();
  
//  pinMode(LEDPIN,OUTPUT);
//  pinMode(LEDPIN,HIGH);
  Serial.begin(9600);
  gsm.begin(&mySerial,9600); //open communicate
  Serial.print("OPERATOR : ");
  
  while(gsm.WaitReady()){Serial.println("Wait");} //wait until gsm module set

  Serial.print("OPERATOR : ");
  Serial.println(gsm.GetOperator()); //Operator -> AIS TRUE DTAC
  Serial.print("Signal Strength");
  Serial.println(gsm.SignalQuality()); //STRENGTH -> Pg22,0 = good,99 = bad

  //finish testing net strength -> set net

  net.DisConnect();
  net.Configure(APN,USER,PASS); //from Pg34
  net.Connect();  //connect net
  Serial.print("IP:");
  Serial.println(net.GetIP());

  mcqtt.callback = callback; //?? Pg50 -> info that sent from server will be add in this function as -> topic,info,size -> everything come as char 

  connect_server(); //start connect server
}

void triggerUC15(){
  pinMode(PWRKEY,OUTPUT);
  digitalWrite(PWRKEY,HIGH);
  delay(3000);
  digitalWrite(PWRKEY,LOW);
}

void callback(String topic ,char *payload,unsigned char length)
{
  Serial.println();
  Serial.println(F("******************"));
  Serial.print(F("Topic = "));
  Serial.println(topic);
  payload[length]=0;
  String str_data(payload);
  Serial.print(F("Payload = "));
  Serial.println(str_data);
}

void connect_server(){

  //Connect server
  
  do{
    Serial.println("Start Connect Server");

    if(mcqtt.DisconnectMQTTServer()){ //Disconnect mqtt server first

      mcqtt.ConnectMQTTServer(MQTT_SERVER,MQTT_PORT); //connect via destinated server and 
    }
    delay(500); //wait for proceed
    Serial.println(mcqtt.ConnectState()); //Show Connection (boolean)
    
  }while(!mcqtt.ConnectState());//until connect succed,keep trying
  
  Serial.println("SERVER CONNECT");

  //login username,password

  unsigned char ret = mcqtt.Connect(MQTT_ID,MQTT_USER,MQTT_PASSWORD);  //change username and password into boolean

  Serial.println(mcqtt.ConnectReturnCode(ret));  //return boolean if it connect or not

  if(ret == 0){ //if connect

    mcqtt.Subscribe("TOPIC");
  }
}

void debug(String data){ //debug address function
  
  Serial.println(data); //see data that have been sent out
}

void loop(){

  unsigned long currentMillis = millis();



  if(currentMillis - previousmqtt >= intervalmqtt)  //every 5 sec publish
  {
       a += 1;
       mcqtt.Publish("TEST_TEST","5",1);        
      previousmqtt = currentMillis; 
  }
  
 
}
