#ifndef UC15
#define UC15

#endif

#if (ARDUNIO >= 100)
  #include "Ardunio.h"

#endif

#include "TEE_UC20.h"
#include "SoftwareSerial.h"
#include <AltSoftSerial.h>
#include "internet.h"
#include "uc_mqtt.h"
#include "stdio.h"
  


class UC15(){

  public:

    //start function
    
    UC15(bool displayMsg=false);

    //get variable
    
    void NET(String APN,String USER,String Pass,int PWRKEY);
    void MQTT(String MQTT_SERVER,String MQTT_PORT,String MQTT_ID,String MQTT_USER,String MQTT_PASSWORD,String Mqtt_Subscriber);
    void callback(String topic ,char *payload,unsigned char length);
    void UPLOAD(String Topic,String Info,boolean Show);

    //setting up Phase
    
  private:

    void UC15SETUP();
    void TriggerUC15();
    void NETSETUP();
    void SERVERSETUP();
    void ConnectServer();
    void Debug(String Data);
    
  
}

