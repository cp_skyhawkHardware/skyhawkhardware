#include "UC15LIB.h"

//create Object For internet
INTERNET net;
UCxMQTT mcqtt;

//net
String APN,USER,PASS;
int PWRKEY;

//mqtt
String MQTT_PASSWORD;
String MQTT_SERVER;
String MQTT_PORT;
String MQTT_ID;
String MQTT_USER;
String MQTT_PASSWORD;
String MQTT_Subscriber;

UC15::UC15(bool displayMsg){
  
  Serial.println("I AM ALIVE");
  _msg = displayMsg;
}

void UC15::NET(String apn,String user,String pass,int pwrkey){

  APN = apn;
  USER = user;
  PASS = pass;
  PWRKEY = pwrkey;
}
void UC15::MQTT(String mqtt_server,String mqtt_port,String mqtt_id,String mqtt_user,String mqtt_password,String Subscriber){

  MQTT_PASSWORD = mqtt_password;
  MQTT_SERVER = mqtt_server;
  MQTT_PORT = mqtt_port;
  MQTT_ID = mqtt_id;
  MQTT_USER = mqtt_user;
  MQTT_PASSWORD = mqtt_password;
  MQTT_Subscriber = Subscriber;
}
void UC15::UC15SETUP(){


  Serial.print("OPERATOR : ");
  TriggerUC15();

  Serial.begin(9600);
  gsm.begin(&mySerial,9600); //open communicate
  Serial.print("OPERATOR : ");

  while(gsm.WaitReady()){Serial.println("Wait");} //wait until gsm module set

  NETSETUP();
  SERVERSETUP();

} 
void UC15::NETSETUP(){

  Serial.print("OPERATOR : ");
  Serial.println(gsm.GetOperator()); //Operator -> AIS TRUE DTAC
  Serial.print("Signal Strength");
  Serial.println(gsm.SignalQuality()); //STRENGTH -> Pg22,0 = good,99 = bad
}
void UC15::SERVERSETUP(){

  net.DisConnect();
  net.Configure(APN,USER,PASS); //from Pg34
  net.Connect();  //connect net
  Serial.print("IP:");
  Serial.println(net.GetIP());

  mcqtt.callback = callback; //?? Pg50 -> info that sent from server will be add in this function as -> topic,info,size -> everything come as char 

  Connect_Server(); //start connect server
}
void UC15::TriggerUC15(){

  pinMode(PWRKEY,OUTPUT);
  digitalWrite(PWRKEY,HIGH);
  delay(3000);
  digitalWrite(PWRKEY,LOW);
}
void UC15::callback(String topic ,char *payload,unsigned char length){

  Serial.println();
  Serial.println(F("******************"));
  Serial.print(F("Topic = "));
  Serial.println(topic);
  payload[length]=0;
  String str_data(payload);
  Serial.print(F("Payload = "));
  Serial.println(str_data);
}
void UC15::ConnectServer(){

  //Connect server
  
  do{
    Serial.println("Start Connect Server");

    if(mcqtt.DisconnectMQTTServer()){ //Disconnect mqtt server first

      mcqtt.ConnectMQTTServer(MQTT_SERVER,MQTT_PORT); //connect via destinated server and 
    }
    delay(500); //wait for proceed
    Serial.println(mcqtt.ConnectState()); //Show Connection (boolean)
    
  }while(!mcqtt.ConnectState());//until connect succed,keep trying
  
  Serial.println("SERVER CONNECT");

  //login username,password

  unsigned char ret = mcqtt.Connect(MQTT_ID,MQTT_USER,MQTT_PASSWORD);  //change username and password into boolean

  Serial.println(mcqtt.ConnectReturnCode(ret));  //return boolean if it connect or not

  if(ret == 0){ //if connect

    mcqtt.Subscribe(MQTT_Subscriber);
  }
}
void UC15::Debug(String data){

  Serial.println(data); //see data that have been sent out
}
void UC15::UPLOAD(String Topic,String Info,boolean Show){


  mcqtt.Publish(Topic,Info,Show);d
}

