/*
 * ServoMotor.h - Library for controlling a servo motor.
 * Created by Nox, SkyHawk Project, April 17, 2018.
 */
#ifndef ServoMotor_h
#define ServoMotor_h

#include "Arduno.h"

class SERVO
{
  public:
    SERVO(int pin);
    int setAngle(int NewAngle);
    int Angle;
    int MinAngle=30
    int MaxAngle=149;
  private:
    int val;
    int Pin;
};

#endif
