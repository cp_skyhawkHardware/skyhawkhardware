/*
 * ServoMotor.cpp - Library for controlling a servo motor.
 * Created by Nox, SkyHawk Project, April 17, 2018.
 */
#include "Arduino.h"
#include "Servo.h"
#include "ServoMotor.h"

Servo servo;

SERVO::SERVO(int pin)
{
  servo.attach(pin);
  Pin = pin;
}

int SERVO::setAngle(int NewAngle)
{
  val = map(val, -90, 89, MinAngle, MaxAngle);
  servo.write(val);
}

