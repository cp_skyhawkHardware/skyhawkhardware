// Controlling a servo position using a potentiometer (variable resistor) 
// by Michal Rinott <http://people.interaction-ivrea.it/m.rinott> 

#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
 

int val;    // variable to read the value from the analog pin 
int dir,mx,mn;

void setup() 
{ 
  myservo.attach(30);  // attaches the servo on pin 9 to the servo object 
  dir = 1;
  mx = 149;
  mn = 30;
} 
 
void loop() 
{ 
  val = 511+511*dir;            // reads the value of the potentiometer (value between 0 and 1023) 
  val = map(val, 0, 1023, mn, mx);     // scale it to use it with the servo (value between 0 and 180) 
  myservo.write(val);                  // sets the servo position according to the scaled value 
  delay(500);   // waits for the servo to get there 
  dir = -dir;
} 