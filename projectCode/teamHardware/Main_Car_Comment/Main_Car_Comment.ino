#include <ServoTimer2.h>

//Read ardujson
//warning
//should disconnect net and mcqtt first then set thing up
#include "Motor.h"
#include "UC15LIB.h"
#include "ACCLERO.h"
#include "RTClib.h"
#include <ArduinoJson.h>
#include <TaskScheduler.h>
#include <Wire.h>

//this part is for GPS Value
//the base code has not yet been check but due to it's limited coding -> we'll put it in anyway
#include "TinyGPS++.h"
#include "SoftwareSerial.h"

SoftwareSerial serial_connection(18, 19); //RX=pin 19, TX=pin 18
TinyGPSPlus gps;    //This is the GPS object that will pretty much do all the grunt work with the NMEA data



//int16_t aX, aY, aZ, gyX, gyY, gyZ, angleX, angleY, angleZ;

UCxMQTT mqtt;
const long reportInterval = 1000;
bool isSystemWasInitiated = false;

//set string tripID to indicate that server and car has successfully connect

String tripID = "";

ACCLERO AC;
COMMUNICATION uc;

//about servo
ServoTimer2 BR1306;
RTC_DS1307 RTC;

//Function prototype
void reportDeviceStatus();

//Task list
//Task run every x/1000 second,up to report interval -> and will run in function reportDeviceStatus
Task reportStatus(reportInterval, TASK_FOREVER, &reportDeviceStatus);  

//Scheduler
Scheduler runner; //as scheudule function to run and check each function that are in schedule

//Motor Driver Setup
MotorDrive mt(3, 4, 5, 6, 7, 8); //declare motor function -> detail later

//MQTT Receive and Handle Example Code : Set throttle for brushless motor
//void MotorSetup() {
//  
//  Serial.println("Setting up motor...");
//  BR1306.attach(9);
//  BR1306.write(700);
//  delay(2000);
//}
void MotorThrottle(int Throttle) {
  
  Serial.println(Throttle);
  mt.motor(Throttle,Throttle); //detail later
}

//set tripID to current run
void InitiateSystem(String newTripID){
  
  tripID = newTripID;
  isSystemWasInitiated = true;  
}
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.serial_connection.begin(9600); //begin GPS
  
  Wire.begin();
  RTC.begin();

  //if RTC is not running set Date and Time again.
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  
  uc.NET("internet", "", "", 42);
  uc.MQTT("35.187.242.237", "1883", "device", "staff", "51<yk@3k2o18", "skyhawkPhase1");

  uc.SETUP(receiveCommand);

  AC.SETUP();

  //initiate runner schedule and add reportstatus in to it's task
  runner.init();
  runner.addTask(reportStatus);
  
  uc.UPLOAD("skyhawkPhase1", "{\"dataType\":\"Greeting\"}", true);
}

void receiveCommand(String topic , char *payload, unsigned char length)
{
  const char* serverCommand;
  payload[length] = 0;

  //set root as json string
  //root is equal to "payload" that come in from server
  
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(payload);
  
  //if root is not error : do this 
 
  if (root.success()) {

    //string servercommand kept command of it's sender before looking into it's detail
    
    serverCommand = root["Command"];
    Serial.print("Server Command: ");
    Serial.println(serverCommand);

    //WTF is this??
    //if string are equal
    
    if (strcmp(serverCommand, "newTripID") == 0) {

      //if string topic equal to "newTripID" set tripID and initiate system
      InitiateSystem(root["Payload"]);
    } 
    if(isSystemWasInitiated){

      //***ALL FUNCTION THAT ARE IN THE CAR GOES TO HERE***
      
       if (strcmp(serverCommand, "Motor") == 0) {

        //Set MotorThrottle
        MotorThrottle(root["Payload"]);
       }

       else if (strcmp(serverCommand, "ServoCam") == 0) {

        //Set Servo Of Camera something
        
       } 
       else if (strcmp(serverCommand, "ServoWheel") == 0) {

        //Set Servo of wheel something
        
       } 
       else if (strcmp(serverCommand, "Cam") == 0) {

        //Set Camera pixel something,or open/close camera something
        
       } 
    }
  }
}

//clear -> this function have GPS and MPU6070 in it,in order to send to server -> run every 1 sec
void reportDeviceStatus() {
  
  String jsonData;
  StaticJsonBuffer <200> jsonBuffer; //make jason string call jsonBuffer have 200 byte in it
  JsonObject& root = jsonBuffer.createObject(); //set root as object to keep jsonBuffer
  
  root["dataType"] = "DeviceStatus";
  JsonObject& payload = root.createNestedObject("payload");

  //send accleration of X,Y,Z grid + Angle Accleration of X,Y,Z grid + Gyro of X,Y,Z grid
  
  payload["flightID"] = tripID;

  //for MPU 6040
  
  payload["aX"] = AC.aX; payload["aY"] = AC.aY; payload["aZ"] = AC.aZ;
  payload["angleX"] = AC.angleX; payload["angleY"] = AC.angleY; payload["angleZ"] = AC.angleZ;
  payload["gyroX"] = AC.gyX; payload["gyroY"] = AC.gyY; payload["gyroZ"] = AC.gyZ;

  //for Future GPS Payload

  payload["Satellite Count"] = gps.satellites.value(); 
  payload["Latitude"] = gps.location.lat(); 
  payload["Longitude"] = gps.location.lng();
  payload["Speed MPH"] = gps.speed.mph(); 
  payload["Altitude Feet"] = gps.altitude.feet(); 
  
  DateTime now = RTC.now(); //set now as this time moment
  payload["timestamp"] = now.unixtime();  //add this moment time to root at topic "timestamp"
  
  root.printTo(jsonData); //add all byte in root to jsonData as string

  
  //  Serial.print("Sending device status to server: ");
  Serial.println(uc.UPLOAD("skyhawkPhase1",jsonData, false));
}

void loop() {
  if(isSystemWasInitiated){
    reportStatus.enable();
  }
  AC.UPDATE();
  runner.execute();
  uc.MqttLoop();

  gps.encode(serial_connection.read()); //Update Value in GPS every Loop
  
  if (!uc.Connectstate())     // check server
    uc.ConnectServer(receiveCommand);

}

