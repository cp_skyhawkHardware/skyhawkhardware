#include "ACCLERO.h"
#include "MPU6050_6Axis_MotionApps20.h"
#include "MPU6050.h"
#define OUTPUT_READABLE_YAWPITCHROLL

MPU6050 mp(0x69);

const int MPU_addr=0x68;
int16_t aX,aY,aZ,gyX,gyY,gyZ, angleX, angleY, angleZ;
bool dmpReady = false; 
uint8_t mpuIntStatus;
uint8_t devStatus; 
uint16_t packetSize;
uint16_t fifoCount; 
uint8_t fifoBuffer[64];
volatile bool mpuInterrupt = false;

Quaternion q; // [w, x, y, z] quaternion container
VectorInt16 aa; // [x, y, z] accel sensor measurements
VectorInt16 aaReal; // [x, y, z] gravity-free accel sensor measurements
VectorInt16 aaWorld; // [x, y, z] world-frame accel sensor measurements
VectorFloat gravity; // [x, y, z] gravity vector
float euler[3]; // [psi, theta, phi] Euler angle container
float ypr[3]; // [yaw, pitch, roll] yaw/pitch/roll container and gravity vector
float gyro[3];  // [x,y,z] gyro value container

uint8_t teapotPacket[14] = { '$', 0x02, 0,0, 0,0, 0,0, 0,0, 0x00, 0x00, '\r', '\n' };

ACCLERO::ACCLERO()
{

  
}

void ACCLERO::SETUP()
{
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
  Wire.begin();
  TWBR = 24; // 400kHz I2C clock (200kHz if CPU is 8MHz)
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
  Fastwire::setup(400, true);
  #endif
  
  Serial.begin(9600);
  while (!Serial); 
  Serial.println(F("Initializing I2C devices..."));
  mp.initialize();
  Serial.println(F("Testing device connections..."));
  Serial.println(mp.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

  Serial.println(F("Initializing DMP..."));
  devStatus = mp.dmpInitialize();
  SETGYRO();
  
  if (devStatus == 0) { 
    
    devStatus_False();
  } else { 
    
    devStatus_True();
  }
}

void ACCLERO::SETGYRO()
{
   // supply your own gyro offsets here, scaled for min sensitivity
  
   mp.setXGyroOffset(220);
   mp.setYGyroOffset(76);
   mp.setZGyroOffset(-85);
   mp.setZAccelOffset(1788); // 1688 factory default for my test chip
}

void ACCLERO::devStatus_True()
{
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));
}
void ACCLERO::incomingData(){
//  Serial.println("Income");
   mpuInterrupt = true;
}
void ACCLERO::devStatus_False()
{
  Serial.println(F("Enabling DMP..."));
    mp.setDMPEnabled(true);

    Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));


//    attachInterrupt(0,reinterpret_cast<void (*)()>(&incomingData), RISING);
    mpuIntStatus = mp.getIntStatus();
    
    Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;
    
    packetSize = mp.dmpGetFIFOPacketSize();
}
void ACCLERO::READ_LINEAR_ACCELERATION(){
  
  mp.dmpGetQuaternion(&q, fifoBuffer);
  mp.dmpGetAccel(&aa, fifoBuffer);
  mp.dmpGetGravity(&gravity, &q);
  mp.dmpGetLinearAccel(&aaReal, &aa, &gravity);
  mp.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &q);
  
  aX = aaWorld.x;
  aY = aaWorld.y;
  aZ = aaWorld.z; 
}
void ACCLERO::READ_ANGLE_ACCELERATION(){
  
  mp.getRotation(&gyX, &gyY, &gyZ);
}
void ACCLERO::READ_ANGLE_TILT(){
  mp.dmpGetQuaternion(&q, fifoBuffer);
  mp.dmpGetGravity(&gravity, &q);
  mp.dmpGetYawPitchRoll(ypr, &q, &gravity);
  angleX = ypr[0] * 180/M_PI;
  angleY = ypr[1] * 180/M_PI;
  angleZ = ypr[2] * 180/M_PI;
}

void ACCLERO::UPDATE()
{
  if (devStatus != 0){
    return;
  }
  mpuInterrupt = false;
  mpuIntStatus = mp.getIntStatus();
//  fifoCount = mp.getFIFOCount();
//  if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
//      mp.resetFIFO();
//      Serial.println(F("FIFO overflow!"));
//  } else if (mpuIntStatus & 0x02) {
      // wait for correct available data length, should be a VERY short wait
      mp.resetFIFO();
      while (fifoCount < packetSize) fifoCount = mp.getFIFOCount();

      // read a packet from FIFO
      mp.getFIFOBytes(fifoBuffer, packetSize);
      
      // track FIFO count here in case there is > 1 packet available
      // (this lets us immediately read more without waiting for an interrupt)
      fifoCount -= packetSize;

     READ_LINEAR_ACCELERATION();
     READ_ANGLE_ACCELERATION();
     READ_ANGLE_TILT();
     
//  }
}


