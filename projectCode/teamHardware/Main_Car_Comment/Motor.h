//it's quite long story but just use it
//circuit not yet correct

#define MOTOERDRIVE_H

#if ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

class MotorDrive
{
public:
  MotorDrive(int pin_ena, int pin_motora_in1, int pin_motora_in2, int pin_enb, int pin_motorb_in1, int pin_motorb_in2)
  {
    pinMode(pin_ena, OUTPUT);
    pinMode(pin_enb,OUTPUT);
    pinMode(pin_motora_in1,OUTPUT);
    pinMode(pin_motora_in2,OUTPUT);
    pinMode(pin_motorb_in1,OUTPUT);
    pinMode(pin_motorb_in2,OUTPUT);

    ena = pin_ena;
    enb = pin_enb;
    a_in1 = pin_motora_in1;
    a_in2 = pin_motora_in2;
    b_in1 = pin_motorb_in1;
    b_in2 = pin_motorb_in2;
  }

  void MotorDrive::motor(int a, int b)
  {
    if (a < 0)
    {
      a *= -1;
      digitalWrite(a_in1, LOW);
      digitalWrite(a_in2, HIGH);
    }
    else
    {
      digitalWrite(a_in1, HIGH);
      digitalWrite(a_in2, LOW);
    }

    if (b < 0)
    {
      b *= -1;
      digitalWrite(b_in1, LOW);
      digitalWrite(b_in2, HIGH);
    }
    else
    {
      digitalWrite(b_in1, HIGH);
      digitalWrite(b_in2, LOW);
    }

    a = (a > 255)? 255 : a;
    b = (b > 255)? 255 : b;
    analogWrite(ena, a);
    analogWrite(enb, b);
  }
protected:
  int ena, enb;
  int a_in1, a_in2;
  int b_in1, b_in2;
};
