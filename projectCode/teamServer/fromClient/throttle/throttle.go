package fromClient

import (
	"../../common"
	"github.com/gin-gonic/gin"
	//"fmt"
	"encoding/json"
	"strconv"

	"github.com/surgemq/message"
)

func SetThrottle(c *gin.Context) {
	throttle_value := calculate_throttle(c.Param("throttle"))
	if throttle_value < 0 {
		c.AbortWithStatus(500)
	} else {
		throttle_json, _ := json.Marshal(map[string]int{"throttle": throttle_value})
		publish_msg := message.NewPublishMessage()
		publish_msg.SetTopic([]byte("skyhawkPhase1"))
		publish_msg.SetPayload([]byte(string(throttle_json)))
		publish_msg.SetQoS(2)
		common.CheckErr(common.MqttListener.Publish(publish_msg, nil))
		//MQTT_connection.Disconnect()
	}
}

func calculate_throttle(input_value string) int {
	raw_value, err := strconv.Atoi(input_value)
	if err != nil {
		raw_value = -1
	}
	output_value := 0
	if raw_value < 0 || raw_value > 100 {
		output_value = -1
	} else if raw_value == 0 {
		output_value = 700
	} else {
		output_value = raw_value*(2500-1550)/100 + 1550
	}
	return output_value
}
