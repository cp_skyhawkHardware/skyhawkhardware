package common

import (
	"encoding/json"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/influxdata/influxdb/client/v2"
	"github.com/surgemq/message"
	"github.com/surgemq/surgemq/service"
	"gopkg.in/mgo.v2"
)

var MqttListener *service.Client

// func InitDB() {
// 	mdb, err := mgo.Dial("mongoDB:27017")
// 	CheckErr(err)
// 	// mdb.DB("admin").Login("root", "n0th1n9N0n53nse")
// 	CheckErr(mdb.DB("SkyhawkPhase1").AddUser("staff", "n0th1n9n0n53n5e", false))
// 	mdb.DB("").
// }
func ConnectMDB() *mgo.Session {
	mdb, err := mgo.Dial("mongoDB:27017")
	CheckErr(err)
	err = mdb.DB("SkyhawkPhase1").Login("staff", "n0th1n9n0n53n5e")
	CheckErr(err)
	return mdb
}
func ConnectInfluxDB() client.Client {
	influxConnnection, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     "http://iotServicePack:8086",
		Username: "staff",
		Password: "n0th1n9n0n53n5e",
	})
	CheckErr(err)
	return influxConnnection
}
func ConnectToMQTTServer() error {
	if MqttListener != nil {
		MqttListener.Disconnect()
	}
	MqttListener = &service.Client{}
	mqttMsg := message.NewConnectMessage()
	mqttMsg.SetUsername([]byte("staff"))
	mqttMsg.SetPassword([]byte("51<yk@3k2o18"))
	mqttMsg.SetWillQos(2)
	mqttMsg.SetVersion(3)
	mqttMsg.SetCleanSession(true)
	mqttMsg.SetClientId([]byte("backend"))
	mqttMsg.SetKeepAlive(60)
	mqttMsg.SetWillTopic([]byte("skyhawkPhase1"))
	mqttMsg.SetWillMessage([]byte("bye"))
	return MqttListener.Connect("tcp://iotServicePack:1883", mqttMsg)
}
func CheckErr(err error) bool {
	if err != nil {
		fmt.Printf("[! - Error]:%s\n", err)
		return true
	}
	return false
}
func RequestFormatter(bytecode []byte) gin.H {
	var jsonStructure gin.H
	CheckErr(json.Unmarshal(bytecode, &jsonStructure))
	return jsonStructure
}
