package main

import (
	"fmt"
	"strings"
	"time"

	"./common"
	"./fromClient/throttle"
	"./fromDevice"
	"github.com/gin-gonic/gin"
	"github.com/surgemq/message"
)

func middleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if strings.TrimSpace(c.PostForm("token")) != "MIICXAIBAAKBgQCtAANo6NhTTTDqfcb3quJPCUG2Tc227KpO1UBOpH0ot3oIq/n0yIr2oWNwiq3bOBSmCQxicCPyvWWHIhyvdJdUDc5W372UQ/5XjUFtrw3Ak0yPnS6LHZ/gExLhpcQ5c9yH8FYPajQTdTMA477QbygD1ZBVWpBFB2yAJNWzQpYBHQIDAQABAoGARg+kUd9ChHHVZpGre2+kGRgJmbfmoLc1vZlVYsPwwAWWEV78amqxBHKVn+U5xXbvvdIaRt9dX5fePxzMey3/fmr2SiLu0o8OuBWy4lTXxB8n5YbqlGVMNxrBWEhljXCOe6hjYWFfcr5nz3577/Ap+Un8hDcUh6AQNRnqNYM/De0CQQDiObQX8xRd1VWeMKzqQm3No1u72ZSM63h3skWrorvykoabsFz2zSmhytMQSNBvn0qAAVs0l36Ou8lGmeecEZtXAkEAw8T34VAzPPdypYdfXKa9C+Yi1C8hczthGy9ZflLHtNq7sRRLSQ9jsBln0kBHx1crv7zPdiDCLuKIETJ5T/3yqwJBAJul/g4YViP1WxIbBW2sROYehkgp/LY0cM9SHfNoZQ4R2IGYB25bXFCQP0XOA3M5UsXdqQX3UcEc3PAxhhRwXMMCQAqG2N1gtqCcnamQ8evFe3zcTxshvaa4lqwji6hjQyHScS1Abhvrm+yoMRD5K+LoKQ4SVQWBFW/39mEWWbeXbCkCQGrYgwy933Ijt0KoxqcYMXUkYZKfm3W+/JZivwOs/Esz923ddEth666K4QcUTapUC/Kq2Og3/F17RrY4li7xbv0=" {
			c.AbortWithStatus(403)
		} else {
			c.Next()
		}
	}
}
func OnReceiveMsg(msg *message.PublishMessage) error {
	jsonData := common.RequestFormatter(msg.Payload())
	switch jsonData["dataType"] {
	case "physicsInfo":
		go print("\n[+] Received physicsInfo from device.")
		fromDevice.LogPhysicsInfo(jsonData)
	default:
		fmt.Printf("[!] Unknown message from device:\"%s\"", msg.Payload())
	}
	return nil
}
func OnCompleteFunc(msg, ack message.Message, err error) error {
	common.CheckErr(err)
	return err
}
func main() {
	// MQTT incoming handle init
	go func() {
		for true {
			common.CheckErr(common.ConnectToMQTTServer())
			submsg := message.NewSubscribeMessage()
			submsg.AddTopic([]byte("skyhawkPhase1"), 2)
			common.CheckErr(common.MqttListener.Subscribe(submsg, OnCompleteFunc, OnReceiveMsg))
			println("Connected.")
			time.Sleep(55 * time.Second)
			print("Reconnecting...")
		}
	}()
	// HTTP API init
	{
		r := gin.Default()
		r.NoRoute(func(c *gin.Context) {
			c.JSON(404, gin.H{
				"err": "404 Not Found",
			})
		})
		r.Use(middleware())
		api := r.Group("api/v1")
		{
			api.POST("/setThrottle/:throttle", fromClient.SetThrottle)
		}
		r.Run(":8080")
	}
}
