package fromDevice

import (
	"time"

	"../common"
	"github.com/gin-gonic/gin"
	"github.com/influxdata/influxdb/client/v2"
)

func LogPhysicsInfo(jsonData gin.H) {
	influxConn := common.ConnectInfluxDB()
	influxTable, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  "SkyhawkPhase1",
		Precision: "ms",
	})
	newRecord, err := client.NewPoint("physicalLog", map[string]string{"flightID": "HardCode101"}, map[string]interface{}{
		"aX":     jsonData["payload"].(map[string]interface{})["aX"],
		"aY":     jsonData["payload"].(map[string]interface{})["aY"],
		"aZ":     jsonData["payload"].(map[string]interface{})["aZ"],
		"angleX": jsonData["payload"].(map[string]interface{})["angleX"],
		"angleY": jsonData["payload"].(map[string]interface{})["angleY"],
		"angleZ": jsonData["payload"].(map[string]interface{})["angleZ"],
	}, time.Now())
	common.CheckErr(err)
	influxTable.AddPoint(newRecord)
	common.CheckErr(influxConn.Write(influxTable))
	common.CheckErr(influxConn.Close())
}
