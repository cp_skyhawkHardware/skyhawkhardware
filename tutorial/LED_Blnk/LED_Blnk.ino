#include <TaskScheduler.h>
#include <PID_v1.h>
void reduceIntervalOnPressButton();
void switchToLED1();
void switchToLED2();
int interval = 2000, led1t, led2t, switcht;
double Input, Output;
PID myPID(&Input, &Output, (double*)&interval,1,1,1, DIRECT);
Task checkButton(50, TASK_FOREVER, &reduceIntervalOnPressButton);
Task ToggleLED1(interval*2, TASK_FOREVER, &switchToLED1);
Task ToggleLED2(interval*2, TASK_FOREVER, &switchToLED2);
Scheduler runner;
//ThreadController controller = ThreadController();
void switchToLED1(){
  digitalWrite(53, LOW);
  digitalWrite(51, HIGH);
  ToggleLED1.setInterval(interval*2);
  led1t = millis();
//  Serial.println("1");
}
void switchToLED2(){
  digitalWrite(51, LOW);
  digitalWrite(53, HIGH);
  ToggleLED2.setInterval(interval*2);
//  ToggleLED2.delay(interval+led1t-millis());
  Input = millis()-led1t;
  myPID.Compute();
  if(Output > 0){
    ToggleLED2.delay(Output); 
  }else{
    ToggleLED1.setInterval();
  }
  Serial.println("<-->");
  Serial.println(interval);
  Serial.println(Input);
  Serial.println(Output);
  Serial.println("</-->");
}
int avgReadAnalog(int port){
  int sum = 0;
  for(int i=0; i<16;i++){
    sum += analogRead(port);
    delay(10);
  }
  return (float)sum/16.0;
}
void reduceIntervalOnPressButton(){
  if(avgReadAnalog(A0)==0){
    interval /= 2;
    digitalWrite(LED_BUILTIN,HIGH);
    delay(1000);
    digitalWrite(LED_BUILTIN,LOW);
//    Serial.println("3");
  }
//  Serial.println("0");
}
void loop() {
  // put your main code here, to run repeatedly:
//  Serial.println("Step01");
//  Blink.run();
//  Serial.println("Step02");
//  checkButton.run();
  runner.execute();
//  delay(500);
}
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(51,OUTPUT);
  pinMode(53,OUTPUT);
  pinMode(A0,INPUT);
  pinMode(LED_BUILTIN,OUTPUT);
  runner.init();
  runner.addTask(checkButton);
  runner.addTask(ToggleLED1);
  runner.addTask(ToggleLED2);
  checkButton.enable();
  ToggleLED1.enable();
  ToggleLED2.enableDelayed(interval);
  led1t = millis();
  myPID.SetMode(AUTOMATIC);
}
