#include "TEE_UC20.h"
#include "SoftwareSerial.h"
#include <AltSoftSerial.h>
#include "internet.h"
#include "uc_mqtt.h"
#include <TaskScheduler.h>
#include "DHT.h"
#include "stdio.h"
INTERNET net;
UCxMQTT mqtt;
//SIM TRUE  internet
#define APN "internet"
#define USER ""
#define PASS ""

#define MQTT_SERVER      "35.188.255.63"
#define MQTT_PORT        "1883"
#define MQTT_ID          "pantarat"
#define MQTT_USER        ""
#define MQTT_PASSWORD    ""
#define MQTT_WILL_TOPIC    0
#define MQTT_WILL_QOS      0
#define MQTT_WILL_RETAIN   0
#define MQTT_WILL_MESSAGE  0

#define BLUE 31
#define YELLOW 33
#define RED 35
#define PWRKEY 42
#define WARN 30
#define ERR 32
unsigned long previousmqtt = 0;
const long intervalmqtt = 1000;
DHT dht(12, DHT11);
AltSoftSerial mySerial;
void debug(String data)
{
  Serial.println(data);
}
void chkSIGNAL(){
  unsigned char signalQuality = gsm.SignalQuality();
  Serial.print("Signal strength=");
  Serial.println(signalQuality);
  if(signalQuality == 99){
    digitalWrite(ERR,HIGH);
    digitalWrite(WARN,LOW);
  }else if(signalQuality < 20){
    digitalWrite(ERR,LOW);
    digitalWrite(WARN,HIGH);
    Serial.println("LOW");
  }else{
    digitalWrite(ERR,LOW);
    digitalWrite(WARN,LOW);
  }
}
void uploadTemperature(){
  char output[100];
  int temp, humid;
  temp = (int)dht.readTemperature();
  humid = (int)dht.readHumidity();
  sprintf(output,"{\"T\":%d, \"H\":%d}",temp,humid);
  mqtt.Publish(String("DHT_TEST"),String(output));
//  Serial.println(output);
}
void testMqttConnection(){
  if(mqtt.ConnectState()){
    return;
  }
  while(!mqtt.ConnectState()){
    Serial.println(F("Reconnect to server..."));
    digitalWrite(ERR,HIGH);
    digitalWrite(WARN,HIGH);
    digitalWrite(BLUE,LOW);
    if(mqtt.DisconnectMQTTServer())
    {
      mqtt.ConnectMQTTServer(MQTT_SERVER,MQTT_PORT);
    }
    delay(200);
    digitalWrite(BLUE,HIGH);
    delay(300);
    Serial.println(mqtt.ConnectState());
  }
  digitalWrite(ERR,LOW);
  digitalWrite(WARN,LOW);
  Serial.println(F("Server Connected"));
  unsigned char ret = mqtt.Connect(MQTT_ID,MQTT_USER,MQTT_PASSWORD);
  digitalWrite(BLUE,ret == 0? HIGH:LOW);
}
Task checkSIGNAL(1000, TASK_FOREVER, &chkSIGNAL);
Task reportTemperature(2000, TASK_FOREVER, &uploadTemperature);
Task testMQTTConnection(500, TASK_FOREVER, &testMqttConnection);
Scheduler runner;
void triggerUC15(){
  pinMode(PWRKEY,OUTPUT);
  digitalWrite(PWRKEY,HIGH);
  delay(3000);
  digitalWrite(PWRKEY,LOW);
}
void turnOnLED(int port){
  digitalWrite(BLUE,LOW);
  digitalWrite(YELLOW,LOW);
  digitalWrite(RED,LOW);
  digitalWrite(port,HIGH);
}
void setup() 
{
  pinMode(BLUE,OUTPUT); // LED - BLUE
  pinMode(YELLOW,OUTPUT); // LED - YELLOW
  pinMode(RED,OUTPUT); // LED - RED
  pinMode(WARN, OUTPUT);
  pinMode(ERR, OUTPUT);
  pinMode(PWRKEY,OUTPUT); // PWRKEY
  Serial.begin(115200);
  Serial.println(F("Initialize"));
  dht.begin();
  triggerUC15();
  gsm.begin(&mySerial,9600);
  gsm.Event_debug = debug;
  while(gsm.WaitReady()){}
  Serial.print(F("GetOperator --> "));
  Serial.println(gsm.GetOperator());
  Serial.print(F("SignalQuality --> "));
  Serial.println(gsm.SignalQuality());
  turnOnLED(RED);
  Serial.println(F("Disconnect net"));
  net.DisConnect();
  Serial.println(F("Set APN and Password"));
  net.Configure(APN,USER,PASS);
  Serial.println(F("Connect net"));
  net.Connect();
  Serial.println(F("Show My IP"));
  Serial.println(net.GetIP());
  turnOnLED(YELLOW);
  mqtt.callback = callback;
  connect_server();
  runner.init();
  runner.addTask(checkSIGNAL);
  runner.addTask(reportTemperature);
  runner.addTask(testMQTTConnection);
  checkSIGNAL.enable();
  reportTemperature.enable();
  testMQTTConnection.enable();
}

void callback(String topic ,char *payload,unsigned char length)
{
  Serial.println();
  Serial.println(F("******************"));
  Serial.print(F("Topic = "));
  Serial.println(topic);
  payload[length]=0;
  String str_data(payload);
  Serial.print(F("Payload = "));
  Serial.println(str_data);
}
void connect_server()
{
  do
  {
     Serial.println(F("Connect Server"));
     Serial.println(F("wait connect"));
      if(mqtt.DisconnectMQTTServer())
      {
        mqtt.ConnectMQTTServer(MQTT_SERVER,MQTT_PORT);
      }
      delay(500);
      Serial.println(mqtt.ConnectState());
  }
  while(!mqtt.ConnectState());
  Serial.println(F("Server Connected"));
  unsigned char ret = mqtt.Connect(MQTT_ID,MQTT_USER,MQTT_PASSWORD);
  digitalWrite(BLUE,ret == 0? HIGH:LOW);
  mqtt.Publish(String("DHT_TEST"),String("hello world"));
  mqtt.Subscribe("DHT_TEST");
  turnOnLED(BLUE);
}
void loop() {
  runner.execute();
  mqtt.MqttLoop();
}
//{
//  unsigned long currentMillis = millis();
//  if(currentMillis - previousmqtt >= intervalmqtt) 
//  {
//      previousmqtt = currentMillis; 
//      if(mqtt.ConnectState()==false)
//      {
//        Serial.println(F("Reconnect"));
//        connect_server();
//      }
//   }
//   mqtt.MqttLoop();
//}
