#include <Servo.h>
#include <stdio.h>
Servo sv01;
char str[50];
int degree = 0;
void setup() {
  // put your setup code here, to run once:
  sv01.attach(7);
  Serial.begin(9600);
  pinMode(24,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  sv01.write(degree%180);
  sprintf(str,"%d\n",degree%180);
  Serial.print(str);
  if(degree%180 == 0){
    digitalWrite(24,HIGH);
    delay(200);
    digitalWrite(24,LOW);
  }
  degree+=10;
  delay(100);
//sv01.write(0);
}
