/*
Coded by Marjan Olesch
Sketch from Insctructables.com
Open source - do what you want with this code!
*/
#include <Servo.h>

Servo firstESC;
int value = 700; // set values you need to zero

void setup() {

  firstESC.attach(9);    // attached to pin 9 I just do this with 1 Servo
  Serial.begin(9600);    // start serial at 9600 baud

}

void loop() {

//First connect your ESC WITHOUT Arming. Then Open Serial and follo Instructions
  firstESC.writeMicroseconds(value);
  Serial.println(value);
  if(Serial.available()){
    int tmp_value = Serial.parseInt();    // Parse an Integer from Serial 
    if(tmp_value >= 700){
      value = tmp_value;
    }
  }
}
