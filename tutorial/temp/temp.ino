#include "DHT.h"
#include "stdio.h"
//#include <Servo.h>
DHT dht(12, DHT11);
//Servo sv01;
void setup() {
  // put your setup code here, to run once:
//  sv01.attach(7);
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  char output[100];
  int temp, humid;
  temp = (int)dht.readTemperature();
  humid = (int)dht.readHumidity();
  sprintf(output,"T=%d, H=%d",temp,humid);
  Serial.println(output);
//  Serial.print(",");
//  sv01.write(180 - ((int)dht.readHumidity()*180/95));
//  delay(50);
}
